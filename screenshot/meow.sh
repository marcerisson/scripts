#!/bin/zsh

source ~/.aliases

# Modified version of
# koin.sh
# AddiKT1ve <the.addikt1ve@gmail.com>

# Output cat
chat (){
cat <<"EOT"
  |\      _,,,--,,_  ,)
  /,`.-'`'   -,  ;-;;'       purr
 |,4-  ) )-,_ ) /\                  purr
'---''(_/--' (_/-'
EOT
}
chat
sleep 1;


# Config
SHOTSDIR=$HOME/images/screenshots
TODAYDIR=`date +%Y-%m-%d`
SHOTNAME=screen-`date +%H-%M-%S`.png
SHOTPATH="$SHOTSDIR/$TODAYDIR/$SHOTNAME"
[[ $1 == "-n" ]] && shift || UPLOADER=upload
UPLOADER_ARGS="screenshots"


# Directory test
[ -d $SHOTSDIR/$TODAYDIR ] || mkdir -p $SHOTSDIR/$TODAYDIR

# Cheese!
if [ -n "$DISPLAY" ]; then
    which scrot > /dev/null 2>&1
    if [ $? -eq 0 ] ; then
        echo "Using scrot"
        scrot -d 0.2 $* $SHOTPATH
    else
        which import > /dev/null 2>&1
        if [ $? -eq 0 ] ; then
            echo "Warning: didn't find scrot, using import instead with no options available"
            import -window root $SHOTPATH
        else
            echo "You should have either scrot or ImageMagick's import installed" >&2
        exit 1
        fi
    fi
else
    which fbgrab > /dev/null 2>&1
    if [ $? -eq 0 ] ; then
            echo "Using fbgrab"
            fbgrab $SHOTPATH
    else
        which fbshot > /dev/null 2>&1
        if [ $? -eq 0 ] ; then
            echo "Using fbshot"
            fbshot $SHOTPATH
        else
            echo "You should have either fbgrab or fbshot installed" >&2
        exit 1
        fi
    fi
fi

echo "Screenshot saved to $SHOTPATH"

# Upload
if [ -n "$UPLOADER" ]; then
    $UPLOADER $SHOTPATH $UPLOADER_ARGS
fi
