MAINSCREEN=DP-1
ALTSCREEN=DVI-I-0

xrandr --output $MAINSCREEN --primary --auto \
       --output $ALTSCREEN --left-of $MAINSCREEN --mode 1280x1024 --rate 70.02  --auto

with_error=`nitrogen --restore 2>&1`
without_error=`nitrogen --restore 2> /dev/null`

if [ "$with_error" != "$without_error" ]
then
    feh  --bg-scale 'FILLME'
fi
